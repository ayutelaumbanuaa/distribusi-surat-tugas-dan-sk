<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require '../database.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// GET DATA FORM REQUEST
$data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
	$apiResponse['status'] = 'true';
	$apiResponse['message'] = "";
	$apiResponse['data'] = null;


if ($data->save_type=='add') {

    // CHECK DATA VALUE IS EMPTY OR NOT
    $insert_query = "INSERT INTO `tag_group` (`nama_group`,`tag`, `created_by`, `created_date`,`updated_by`,`updated_date`) VALUES (:nama_group, :tag, :created_by, :created_date, :updated_by, :updated_date);"; 
    
    $insert_stmt = $conn->prepare($insert_query);
    // DATA BINDING
    $insert_stmt->bindValue(':nama_group', htmlspecialchars(strip_tags($data->nama_group)),PDO::PARAM_STR);
    $insert_stmt->bindValue(':tag', htmlspecialchars(strip_tags($data->tag)),PDO::PARAM_STR);
    $insert_stmt->bindValue(':created_by', htmlspecialchars(strip_tags($data->created_by)),PDO::PARAM_STR);
    $insert_stmt->bindValue(':created_date', htmlspecialchars(strip_tags($data->created_date)),PDO::PARAM_STR);
    $insert_stmt->bindValue(':updated_by', htmlspecialchars(strip_tags($data->updated_by)),PDO::PARAM_STR);
    $insert_stmt->bindValue(':updated_date', htmlspecialchars(strip_tags($data->updated_date)),PDO::PARAM_STR);
    
    if($insert_stmt->execute()){
        $apiResponse['status'] = 'true';
        $apiResponse['message'] = 'Data Inserted Successfully';
    }else{		
        $apiResponse['status'] = 'true';
        $apiResponse['message'] = 'Data not Inserted';
        }       
    }

else if ($data->save_type=='update') {
    
    $check_post = "SELECT * FROM `tag_group` WHERE id=:post_id";
    $check_post_stmt = $conn->prepare($check_post);
    $check_post_stmt->bindValue(':post_id', $data->id,PDO::PARAM_INT);
    $check_post_stmt->execute();
    $update_query ="";
        $update_query = "UPDATE `tag_group` SET `nama_group` = :nama_group, `tag` = :tag, `updated_by` = :updated_by, `updated_date` = :updated_date WHERE `tag_group`.`id` = :id"; 
    $update_stmt = $conn->prepare($update_query);
    // DATA BINDING

    $update_stmt->bindValue(':nama_group', htmlspecialchars(strip_tags($data->nama_group)),PDO::PARAM_STR);
    $update_stmt->bindValue(':tag', htmlspecialchars(strip_tags($data->tag)),PDO::PARAM_STR);
    $update_stmt->bindValue(':updated_by', htmlspecialchars(strip_tags($data->updated_by)),PDO::PARAM_STR);
    $update_stmt->bindValue(':updated_date', htmlspecialchars(strip_tags($data->updated_date)),PDO::PARAM_STR);
    $update_stmt->bindValue(':id', htmlspecialchars(strip_tags($data->id)),PDO::PARAM_STR);

    if($update_stmt->execute()){
        $apiResponse['status'] = 'success';
        $apiResponse['message'] = 'Data Saved Successfully';
        $apiResponse['data'] =null; 
    }else{      
        $apiResponse['status'] = 'failed';
        $apiResponse['message'] = 'Data not Saved';
    } 
    
}
else{
	$apiResponse['status'] = 'true';
    $apiResponse['message'] = 'Please fill all the fields |';
    }

//ECHO DATA IN JSON FORMAT
echo  json_encode($apiResponse);
?>
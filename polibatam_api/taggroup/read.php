<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
require '../database.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

$apiResponse['status'] = 'success';
$apiResponse['message'] = "";
$apiResponse['data'] = null;
$data = json_decode(file_get_contents("php://input"));
if(isset($data->id)){
//IF HAS ID PARAMETER
    $id = filter_var($data->id, FILTER_VALIDATE_INT,[
        'options' => [
            'default' => 'all_posts',
            'min_range' => 1
        ]
    ]);
}
else {
    $id = 'all_posts';
}

// MAKE SQL QUERY
// IF GET POSTS ID, THEN SHOW POSTS BY ID OTHERWISE SHOW ALL POSTS
$sql = is_numeric($id) ? "SELECT * FROM tag_group WHERE id=$id" : "SELECT * FROM tag_group"; 
$stmt = $conn->prepare($sql);
$stmt->execute();

//CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
if($stmt->rowCount() > 0){
    // CREATE POSTS ARRAY
    $posts_array = [];
    
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        $post_data = [
            'id' => $row['id'],
            'nama_group' => $row['nama_group'],
            'tag' => $row['tag'],
            'created_by' => $row['created_by'],
            'created_date' => $row['created_date'],
            'updated_by' => $row['updated_by'],
            'updated_date' => $row['updated_date']
        ];
        // PUSH POST DATA IN OUR $posts_array ARRAY
		if(isset($data->orderid)){
			$apiResponse['data']=$post_data;
		}
		else {
			array_push($posts_array, $post_data);
		}
    }
    //SHOW POST/POSTS IN JSON FORMAT
	if(!isset($data->id)){
		$apiResponse['data']=$posts_array;
	}
	echo json_encode($apiResponse);
}
else{
	$apiResponse['status'] = 'failed';
	$apiResponse['message'] = "No Data Found";
	$apiResponse['data'] = null;
    //IF THER IS NO POST IN OUR DATABASE
    echo json_encode($apiResponse);
}
?>
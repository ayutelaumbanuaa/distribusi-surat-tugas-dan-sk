<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// GET DATA FORM REQUEST
$data = json_decode(file_get_contents("php://input"));

//CHECKING, IF ID AVAILABLE ON $data
if(isset($data->username) && isset($data->password)){
    $username = $data->username;
    $password = $data->password;
	$curl = curl_init();
	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'http://sid.polibatam.ac.id/apilogin/web/api/auth/login',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'POST',
	  CURLOPT_POSTFIELDS => array('username' => $username,'password' => $password,'token' => 'lU0y3PED8IQpNowtKebUd3LkUdKO4y7X'),
	));

	$response = curl_exec($curl);
	curl_close($curl);
    echo  $response;
    
}
?>
<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");

// INCLUDING DATABASE AND MAKING OBJECT
require '../database.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

$apiResponse['status'] = 'success';
$apiResponse['message'] = "";
$apiResponse['data'] = null;
$data = json_decode(file_get_contents("php://input"));
if(isset($data->id)){
//IF HAS ID PARAMETER
    $id = filter_var($data->id, FILTER_VALIDATE_INT,[
        'options' => [
            'default' => 'all_posts',
            'min_range' => 1
        ]
    ]);
}
else {
    $id = 'all_posts';
}

$uid=$data->uid;
// MAKE SQL QUERY
// IF GET POSTS ID, THEN SHOW POSTS BY ID OTHERWISE SHOW ALL POSTS
$sql = is_numeric($id) ? "SELECT * FROM `surat_keputusan` WHERE id='$id' AND (created_by='$uid' OR tag like '%$uid%')"  : "SELECT * FROM `surat_keputusan` where created_by='$uid' OR tag like '%$uid%'"; 
$stmt = $conn->prepare($sql);
$stmt->execute();

//CHECK WHETHER THERE IS ANY POST IN OUR DATABASE
if($stmt->rowCount() > 0){
    // CREATE POSTS ARRAY
    $posts_array = [];
    
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        $post_data = [
            'id' => $row['id'],
            'nama' => $row['nama'],
            'file_path' => $row['file_path'],
            'deskripsi' => $row['deskripsi'],
            'tag' => $row['tag'],
            'created_by' => $row['created_by'],
            'created_date' => $row['created_date'],
            'updated_by' => $row['updated_by'],
            'updated_date' => $row['updated_date']
        ];
        // PUSH POST DATA IN OUR $posts_array ARRAY
        if(isset($data->orderid)){
            $apiResponse['data']=$post_data;
        }
        else {
            array_push($posts_array, $post_data);
        }
    }
    //SHOW POST/POSTS IN JSON FORMAT
    if(!isset($data->id)){
        $apiResponse['data']=$posts_array;
    }
    echo json_encode($apiResponse);
}
else{
    $apiResponse['status'] = 'failed';
    $apiResponse['message'] = "No Data Found";
    $apiResponse['data'] = null;
    //IF THER IS NO POST IN OUR DATABASE
    echo json_encode($apiResponse);
}
?>
<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// default api response
$apiResponse['status'] = 'success';
$apiResponse['message'] = "";
$apiResponse['data'] = null;

require '../database.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

$save_type = $_POST['save_type'];
$id = $_POST['id'];
$file_path = $_POST['file_path'];
$nama = $_POST['nama'];
$deskripsi = $_POST['deskripsi'];
$tag = $_POST['tag'];
$created_by = $_POST['created_by'];
$created_date = $_POST['created_date'];
$updated_by = $_POST['updated_by'];
$updated_date = $_POST['updated_date'];
$formated_date = $_POST['formated_date'];
$info="";
$ext="";
if(array_key_exists('fileToUpload', $_FILES)){
	$info = pathinfo($_FILES['fileToUpload']['name']);
	$ext = $info['extension']; 
}
$file_name = $nama.$formated_date.'.'.$ext;
$file_path ='/documents/'.$file_name;

if ($save_type=='add') {
	$insert_query = "INSERT INTO `surat_keputusan` (`nama`, `file_path`, `deskripsi`, `tag`, `created_by`, `created_date`,`updated_by`,`updated_date`) VALUES (:nama, :file_path, :deskripsi, :tag, :created_by, :created_date, :updated_by, :updated_date);"; 
	$insert_stmt = $conn->prepare($insert_query);
	// DATA BINDING
	$insert_stmt->bindValue(':nama', htmlspecialchars(strip_tags($nama)),PDO::PARAM_STR);
	$insert_stmt->bindValue(':file_path', htmlspecialchars(strip_tags($file_path)),PDO::PARAM_STR);
	$insert_stmt->bindValue(':deskripsi', htmlspecialchars(strip_tags($deskripsi)),PDO::PARAM_STR);
	$insert_stmt->bindValue(':tag', htmlspecialchars(strip_tags($tag)),PDO::PARAM_STR);
	$insert_stmt->bindValue(':created_by', htmlspecialchars(strip_tags($created_by)),PDO::PARAM_STR);
	$insert_stmt->bindValue(':created_date', htmlspecialchars(strip_tags($created_date)),PDO::PARAM_STR);
	$insert_stmt->bindValue(':updated_by', htmlspecialchars(strip_tags($updated_by)),PDO::PARAM_STR);
	$insert_stmt->bindValue(':updated_date', htmlspecialchars(strip_tags($updated_date)),PDO::PARAM_STR);

	if($insert_stmt->execute()){
		// upload file
		$target_dir = "documents/";
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		//$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
		move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "../documents/".$file_name);
		$id = $conn->lastInsertId();
		$apiResponse['status'] = 'success';
		$apiResponse['message'] = 'Data Saved Successfully';
		$apiResponse['data'] = $id;	
	}else{		
		$apiResponse['status'] = 'failed';
		$apiResponse['message'] = 'Data not Saved';
	} 
}
else if ($save_type=='update') {
	$check_post = "SELECT * FROM `surat_keputusan` WHERE id=:post_id";
    $check_post_stmt = $conn->prepare($check_post);
    $check_post_stmt->bindValue(':post_id', $id,PDO::PARAM_INT);
    $check_post_stmt->execute();
	if($check_post_stmt->rowCount() > 0){
        while($row = $check_post_stmt->fetch(PDO::FETCH_ASSOC)){
			$filepathdeleted = $row['file_path'];
			if (file_exists('..'.$filepathdeleted)) {
			   unlink('..'.$filepathdeleted);
			}
		}
	}
	$update_query ="";
	if ($ext=="") {
		$update_query = "UPDATE `surat_keputusan` SET `nama` = :nama, `deskripsi` = :deskripsi, `tag` = :tag, `updated_by` = :updated_by, `updated_date` = :updated_date WHERE `surat_keputusan`.`id` = :id"; 
	}
	else {
		$update_query = "UPDATE `surat_keputusan` SET `nama` = :nama, `file_path` = :file_path, `deskripsi` = :deskripsi, `tag` = :tag, `updated_by` = :updated_by, `updated_date` = :updated_date WHERE `surat_keputusan`.`id` = :id"; 
	}
	$update_stmt = $conn->prepare($update_query);
	// DATA BINDING
	$update_stmt->bindValue(':nama', htmlspecialchars(strip_tags($nama)),PDO::PARAM_STR);
	if ($ext!="") {
		$update_stmt->bindValue(':file_path', htmlspecialchars(strip_tags($file_path)),PDO::PARAM_STR);
	}
	$update_stmt->bindValue(':deskripsi', htmlspecialchars(strip_tags($deskripsi)),PDO::PARAM_STR);
	$update_stmt->bindValue(':tag', htmlspecialchars(strip_tags($tag)),PDO::PARAM_STR);
	$update_stmt->bindValue(':updated_by', htmlspecialchars(strip_tags($updated_by)),PDO::PARAM_STR);
	$update_stmt->bindValue(':updated_date', htmlspecialchars(strip_tags($updated_date)),PDO::PARAM_STR);
	$update_stmt->bindValue(':id', htmlspecialchars(strip_tags($id)),PDO::PARAM_STR);

	if($update_stmt->execute()){
		// upload file
		if ($ext!="") {
			$target_dir = "documents/";
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			// Check if image file is a actual image or fake image
			//$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "../documents/".$file_name);
		}
		$apiResponse['status'] = 'success';
		$apiResponse['message'] = 'Data Saved Successfully';
		$apiResponse['data'] =null;	
	}else{		
		$apiResponse['status'] = 'failed';
		$apiResponse['message'] = 'Data not Saved';
	} 
}


echo  json_encode($apiResponse);
?>
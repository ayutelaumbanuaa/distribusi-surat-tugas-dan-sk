<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// default api response
$apiResponse['status'] = 'true';
$apiResponse['message'] = "";
$apiResponse['data'] = null;

require '../database.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

if ($save_type=='add') {
	$insert_query = "INSERT INTO `management` (`tag`, `isadmin`, `created_by`, `created_date`,`updated_by`,`updated_date`) VALUES (:tag, :isadmin, :created_by, :created_date, :updated_by, :updated_date);"; 
	$insert_stmt = $conn->prepare($insert_query);
	// DATA BINDING
	$insert_stmt->bindValue(':tag', htmlspecialchars(strip_tags($tag)),PDO::PARAM_STR);
	$insert_stmt->bindValue(':isadmin', htmlspecialchars(strip_tags($nama)),PDO::PARAM_STR);
	$insert_stmt->bindValue(':created_by', htmlspecialchars(strip_tags($created_by)),PDO::PARAM_STR);
	$insert_stmt->bindValue(':created_date', htmlspecialchars(strip_tags($created_date)),PDO::PARAM_STR);
	$insert_stmt->bindValue(':updated_by', htmlspecialchars(strip_tags($updated_by)),PDO::PARAM_STR);
	$insert_stmt->bindValue(':updated_date', htmlspecialchars(strip_tags($updated_date)),PDO::PARAM_STR);

	if($insert_stmt->execute()){
	
		$apiResponse['status'] = 'success';
		$apiResponse['message'] = 'Data Saved Successfully';
		$apiResponse['data'] = $id;	
	}else{		
		$apiResponse['status'] = 'failed';
		$apiResponse['message'] = 'Data not Saved';
	} 
}
else if ($save_type=='update') {
	$check_post = "SELECT * FROM `management` WHERE id=:post_id";
    $check_post_stmt = $conn->prepare($check_post);
    $check_post_stmt->bindValue(':post_id', $id,PDO::PARAM_INT);
    $check_post_stmt->execute();

	$update_query ="";
		$update_query = "UPDATE `management` SET `isadmin` = :isadmin, `tag` = :tag, `updated_by` = :updated_by, `updated_date` = :updated_date WHERE `management`.`id` = :id"; 
=
	$update_stmt = $conn->prepare($update_query);
	// DATA BINDING

	$update_stmt->bindValue(':tag', htmlspecialchars(strip_tags($tag)),PDO::PARAM_STR);
	$update_stmt->bindValue(':isadmin', htmlspecialchars(strip_tags($isadmin)),PDO::PARAM_STR);
	$update_stmt->bindValue(':updated_by', htmlspecialchars(strip_tags($updated_by)),PDO::PARAM_STR);
	$update_stmt->bindValue(':updated_date', htmlspecialchars(strip_tags($updated_date)),PDO::PARAM_STR);
	$update_stmt->bindValue(':id', htmlspecialchars(strip_tags($id)),PDO::PARAM_STR);

	if($update_stmt->execute()){
        $apiResponse['status'] = 'success';
        $apiResponse['message'] = 'Data Saved Successfully';
        $apiResponse['data'] =null; 
    }else{      
        $apiResponse['status'] = 'failed';
        $apiResponse['message'] = 'Data not Saved';
    } 
    
}
else{
	$apiResponse['status'] = 'true';
    $apiResponse['message'] = 'Please fill all the fields |';
    }
echo  json_encode($apiResponse);
?>
-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 02, 2022 at 05:46 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `polibatam`
--

-- --------------------------------------------------------

--
-- Table structure for table `management`
--

CREATE TABLE `management` (
  `id` int(11) NOT NULL,
  `tag` text NOT NULL,
  `isadmin` text NOT NULL,
  `created_by` text NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` text NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `management`
--

INSERT INTO `management` (`id`, `tag`, `isadmin`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, '3312011001', 'Admin', '3312011001', '2022-06-13 11:43:05', '3312011001', '2022-06-16 12:08:37'),
(2, '3312011005', 'Dosen', '3312011001', '2022-06-13 11:43:21', '3312011001', '2022-06-13 11:43:21'),
(7, '199102032018031001', 'Admin', '3312011001', '2022-07-02 10:30:21', '3312011001', '2022-07-02 10:30:21');

-- --------------------------------------------------------

--
-- Table structure for table `surat_keputusan`
--

CREATE TABLE `surat_keputusan` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `file_path` text NOT NULL,
  `deskripsi` text NOT NULL,
  `tag` text NOT NULL,
  `created_by` text NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` text NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `surat_keputusan`
--

INSERT INTO `surat_keputusan` (`id`, `nama`, `file_path`, `deskripsi`, `tag`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, '13/SK-V/2022', '/documents/13/SK-V/202220220702104247.pdf', 'Surat Keputusan Penerimaan', '198511242020121001||199007122021211002||198610092021211001', '3312011001', '2022-07-02 10:40:28', '3312011001', '2022-07-02 10:42:47');

-- --------------------------------------------------------

--
-- Table structure for table `surat_tugas`
--

CREATE TABLE `surat_tugas` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `file_path` text NOT NULL,
  `deskripsi` text NOT NULL,
  `tag` text NOT NULL,
  `created_by` text NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` text NOT NULL,
  `updated_date` datetime NOT NULL DEFAULT '2022-01-01 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `surat_tugas`
--

INSERT INTO `surat_tugas` (`id`, `nama`, `file_path`, `deskripsi`, `tag`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, '12/ST-VI/2022', '/documents/12/ST-VI/202220220702103826.pdf', 'Surat Tugas Pengabdian Masyarakat', '198804252021212001||214176||199102032018031001', '3312011001', '2022-07-02 10:37:14', '3312011001', '2022-07-02 10:38:26');

-- --------------------------------------------------------

--
-- Table structure for table `tag_group`
--

CREATE TABLE `tag_group` (
  `id` int(11) NOT NULL,
  `nama_group` text NOT NULL,
  `tag` text NOT NULL,
  `created_by` text NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` text NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `management`
--
ALTER TABLE `management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surat_keputusan`
--
ALTER TABLE `surat_keputusan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surat_tugas`
--
ALTER TABLE `surat_tugas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag_group`
--
ALTER TABLE `tag_group`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `management`
--
ALTER TABLE `management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `surat_keputusan`
--
ALTER TABLE `surat_keputusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `surat_tugas`
--
ALTER TABLE `surat_tugas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tag_group`
--
ALTER TABLE `tag_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

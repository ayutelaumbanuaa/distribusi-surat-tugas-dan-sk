import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_chips_input/flutter_chips_input.dart';
import 'package:applikasi_sk/api_services/LDAPServices.dart';
import 'package:applikasi_sk/api_services/ManagementUserServices.dart';
import 'package:applikasi_sk/helper/dialogs.dart';
import 'package:applikasi_sk/models/data_dosen_model.dart';
import 'package:applikasi_sk/models/management_user_model.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../../api_services/TagGroupServices.dart';
import '../../helper/color.dart';
import '../../helper/constanta.dart';
import '../../helper/responsive.dart';
import '../../models/apiresponse.dart';
import '../../models/tag_group_model.dart';
import '../widgets/header.dart';

class TagGroupPage extends StatefulWidget {
  const TagGroupPage({Key? key}) : super(key: key);
  @override
  State<TagGroupPage> createState() => _TagGroupPageState();
}

class _TagGroupPageState extends State<TagGroupPage> {
  bool isLoading = false;
  late List<TagGroupModel> listTagGroup = [];
  late List<DataDosenModel> listDataDosen = [];
  int? sortColumnIndex;
  bool isAscending = false;
  final columns = ['ID', 'Nama Group', 'Tag', 'Created By'];
  final columnsMobile = ['Nama Group', 'Tag'];
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String currentUid = '';
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  List<Widget> listScreen = [];
  int currentMenu = 0;
  late List<ManagementUserModel> listManagementUser = [];

  void initDataUser() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    currentUid = prefs.getString(Constanta.keyUserId)!;
    // print(currentUid);

    listManagementUser = [];
    Map map = {"uid": currentUid};
    var requestBody = jsonEncode(map);
    ManagementUserServices.getDataManagementUser(requestBody).then((result) {
      if (result.status.toLowerCase() == "success") {
        listManagementUser = List<ManagementUserModel>.from(
            result.data.map((x) => ManagementUserModel.fromJson(x)));

        setState(() {
          isLoading = false;
        });
        print(listManagementUser
            .firstWhere((element) => element.tag == currentUid)
            .isAdmin);
      } else {
        //Dialogs.showAlertMessage(result.message, context);
      }
    }).onError((error, stackTrace) {
      Dialogs.showAlertMessage(
          error.toString() + stackTrace.toString(), context);
    });
  }

  List<DataColumn> getColumns(List<String> columns) => columns
      .map((String column) => DataColumn(
            label: Text(
              column,
              style: const TextStyle(color: Colors.white),
            ),
            onSort: onSort,
          ))
      .toList();

  List<DataRow> getRowsMobile(List<TagGroupModel> objects) =>
      objects.map((TagGroupModel object) {
        var index = objects.indexOf(object);
        final cells = [object.namaGroup, object.tag];
        List<DataCell> listDataCell = getCells(cells);

        return DataRow(
            color: index % 2 == 0
                ? MaterialStateColor.resolveWith(
                    (states) => AppColors.tableCellOddColor)
                : MaterialStateColor.resolveWith(
                    (states) => AppColors.tableCellEvenColor),
            onSelectChanged: (bool? selected) async {
              if (selected!) {
                await showFormDialog(context, false, object);
              }
            },
            cells: listDataCell);
      }).toList();

  List<DataRow> getRows(List<TagGroupModel> objects) =>
      objects.map((TagGroupModel object) {
        var index = objects.indexOf(object);
        final cells = [
          object.id,
          object.namaGroup,
          object.tag,
          object.createdBy
        ];
        List<DataCell> listDataCell = getCells(cells);

        return DataRow(
            color: index % 2 == 0
                ? MaterialStateColor.resolveWith(
                    (states) => AppColors.tableCellOddColor)
                : MaterialStateColor.resolveWith(
                    (states) => AppColors.tableCellEvenColor),
            onSelectChanged: (bool? selected) async {
              if (selected!) {
                await showFormDialog(context, false, object);
              }
            },
            cells: listDataCell);
      }).toList();

  List<DataCell> getCells(List<dynamic> cells) =>
      cells.map((data) => DataCell(Text('$data'))).toList();

  void onSort(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      listTagGroup.sort((obj1, obj2) =>
          compareString(ascending, obj1.id.toString(), obj2.id.toString()));
    } else if (columnIndex == 1) {
      listTagGroup.sort((obj1, obj2) =>
          compareString(ascending, obj1.namaGroup!, obj2.namaGroup!));
    } else if (columnIndex == 2) {
      listTagGroup
          .sort((obj1, obj2) => compareString(ascending, obj1.tag!, obj2.tag!));
    } else if (columnIndex == 3) {
      listTagGroup.sort((obj1, obj2) =>
          compareString(ascending, obj1.createdBy!, obj2.createdBy!));
    }
    setState(() {
      sortColumnIndex = columnIndex;
      isAscending = ascending;
    });
  }

  int compareString(bool ascending, String value1, String value2) =>
      ascending ? value1.compareTo(value2) : value2.compareTo(value1);

  Future<void> showFormDialog(
      BuildContext context, bool isNewData, TagGroupModel model) async {
    var isAdminKah = listManagementUser
            .firstWhere((element) => element.tag == currentUid)
            .isAdmin ??
        'Dosen';

    return isAdminKah == 'Admin'
        ? await showDialog(
            barrierDismissible: false,
            context: context,
            builder: (context) {
              bool isLoading = false;
              final _nameController = TextEditingController();
              final _tagController = TextEditingController();
              late List<DataDosenModel> listTag = [];
              if (!isNewData) {
                var parts = model.tag!.split('||');
                for (var niknimunit in parts) {
                  listTag.add(listDataDosen
                      .firstWhere((data) => data.nimNikUnit == niknimunit));
                }
              }
              if (!isNewData) _nameController.text = model.namaGroup!;
              if (!isNewData) _tagController.text = model.tag!;
              return StatefulBuilder(builder: (context, setState) {
                return AlertDialog(
                  insetPadding: const EdgeInsets.all(8),
                  content: SizedBox(
                    width: Responsive.isDesktop(context)
                        ? MediaQuery.of(context).size.width * 0.6
                        : Responsive.isTablet(context)
                            ? MediaQuery.of(context).size.width * 0.8
                            : MediaQuery.of(context).size.width,
                    child: Form(
                        key: _formKey,
                        child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Data Group",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 24),
                              ),
                              const SizedBox(height: 18),
                              TextFormField(
                                controller: _nameController,
                                validator: (value) {
                                  return value!.isNotEmpty
                                      ? null
                                      : "Invalid Field";
                                },
                                decoration: const InputDecoration(
                                    hintText: "Enter Some Text",
                                    labelText: "Nama Group"),
                              ),
                              const SizedBox(height: 12),
                              ChipsInput(
                                initialValue: isNewData ? [] : listTag,
                                decoration: InputDecoration(
                                  labelText: "Select People to tag",
                                ),
                                maxChips: 100,
                                findSuggestions: (String query) {
                                  if (query.length != 0) {
                                    var lowercaseQuery = query.toLowerCase();
                                    return listDataDosen.where((profile) {
                                      return profile.name
                                          .toLowerCase()
                                          .contains(query.toLowerCase());
                                    }).toList(growable: false)
                                      ..sort((a, b) => a.name
                                          .toLowerCase()
                                          .indexOf(lowercaseQuery)
                                          .compareTo(b.name
                                              .toLowerCase()
                                              .indexOf(lowercaseQuery)));
                                  } else {
                                    return const <DataDosenModel>[];
                                  }
                                },
                                onChanged: (listdata) {
                                  _tagController.text = "";
                                  for (var element in listdata) {
                                    DataDosenModel data =
                                        element as DataDosenModel;
                                    if (_tagController.text == "") {
                                      _tagController.text = data.nimNikUnit;
                                    } else {
                                      _tagController.text =
                                          _tagController.text +
                                              "||" +
                                              data.nimNikUnit;
                                    }
                                  }

                                  // print(_tagController.text);
                                },
                                chipBuilder: (context, state, profile) {
                                  DataDosenModel model =
                                      profile as DataDosenModel;
                                  return InputChip(
                                    key: ObjectKey(profile),
                                    label: Text(model.name),
                                    onDeleted: () => state.deleteChip(profile),
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                  );
                                },
                                suggestionBuilder: (context, state, profile) {
                                  DataDosenModel model =
                                      profile as DataDosenModel;
                                  return ListTile(
                                    key: ObjectKey(profile),
                                    title: Text(model.name),
                                    subtitle: Text(model.nimNikUnit),
                                    onTap: () =>
                                        state.selectSuggestion(profile),
                                  );
                                },
                              ),
                            ])),
                  ),
                  actions: <Widget>[
                    if (!isNewData)
                      TextButton(
                        child: const Text(
                          'Delete',
                          style: TextStyle(color: Colors.red),
                        ),
                        onPressed: () async {
                          bool? isValid = await Dialogs.showOptionsDialog(
                              "Are you sure to delete this data?",
                              "YES",
                              "NO",
                              false,
                              context);
                          if (isValid!) {
                            Navigator.of(context).pop();
                            var requestBody = jsonEncode(model.toJson());
                            TagGroupServices.deleteDataTagGroup(requestBody)
                                .then((result) {
                              if (result.status.toLowerCase() == 'success') {
                                Dialogs.showAlertMessage(
                                    result.message, context);
                              }
                            }).whenComplete(() {
                              initData();
                            });
                          }
                        },
                      ),
                    TextButton(
                      child: const Text('Cancel'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    isLoading
                        ? const CircularProgressIndicator()
                        : TextButton(
                            child: Text(isNewData ? 'Add' : 'Update'),
                            onPressed: () async {
                              if (_formKey.currentState!.validate()) {
                                if (!mounted) return;
                                setState(() {
                                  isLoading = true;
                                });

                                var request = http.MultipartRequest(
                                    'POST',
                                    Uri.parse(Constanta.apiUrl +
                                        '/taggroup/save.php'));
                                TagGroupModel tagGroupModel = TagGroupModel(
                                    saveType: isNewData ? 'add' : 'update',
                                    id: isNewData ? '' : model.id!,
                                    createdBy: currentUid,
                                    namaGroup: _nameController.text,
                                    tag: _tagController.text,
                                    updatedBy: currentUid,
                                    createdDate: DateTime.now(),
                                    updatedDate: DateTime.now());
                                var requestBody =
                                    jsonEncode(tagGroupModel.toJson());
                                print(requestBody);
                                TagGroupServices.saveDataTagGroup(requestBody)
                                    .then((result) {
                                  // print(result.toJson());
                                });

                                http.StreamedResponse response =
                                    await request.send();
                                setState(() {
                                  isLoading = false;
                                });
                                if (response.statusCode == 200) {
                                  initData();
                                  Navigator.of(context).pop();
                                  Dialogs.showAlertMessage(
                                      "Data successfully update!", context);
                                  print(await response.stream.bytesToString());
                                  ApiResponse apiResponse = apiResponseFromJson(
                                      await response.stream.bytesToString());
                                  Dialogs.showAlertMessage(
                                      "" + apiResponse.message, context);
                                } else {
                                  Dialogs.showAlertMessage(
                                      "Document Error : " +
                                          response.reasonPhrase!,
                                      context);
                                }
                              }
                            },
                          ),
                  ],
                );
              });
            })
        : Dialogs.showAlertMessage("Anda Bukan Admin", context);
  }

  @override
  void initState() {
    initData();
    initDataUser();
    super.initState();
  }

  void initData() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    currentUid = prefs.getString(Constanta.keyUserId)!;
    print(currentUid);

    listTagGroup = [];
    Map map = {"uid": currentUid};
    var requestBody = jsonEncode(map);
    TagGroupServices.getDataTagGroup(requestBody).then((result) {
      setState(() {
        isLoading = false;
      });
      if (result.status.toLowerCase() == "success") {
        listTagGroup = List<TagGroupModel>.from(
            result.data.map((x) => TagGroupModel.fromJson(x)));
      } else {
        //Dialogs.showAlertMessage(result.message, context);
      }
    }).onError((error, stackTrace) {
      Dialogs.showAlertMessage(
          error.toString() + stackTrace.toString(), context);
    });

    LDAPServices.getListDataDosen(requestBody).then((result) {
      setState(() {
        isLoading = false;
      });
      if (result.status.toLowerCase() == "success") {
        listDataDosen = List<DataDosenModel>.from(
            result.data.map((x) => DataDosenModel.fromJson(x)));
      } else {
        //Dialogs.showAlertMessage(result.message, context);
      }
    }).onError((error, stackTrace) {
      Dialogs.showAlertMessage(
          error.toString() + stackTrace.toString(), context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Header(
                    title: 'Document Group',
                    subTitle: '',
                  ),
                  ElevatedButton.icon(
                    label: Text(
                      'Tambahkan Group',
                    ),
                    icon: Icon(Icons.add),
                    style: ElevatedButton.styleFrom(
                      primary: Color.fromARGB(255, 74, 127, 207),
                    ),
                    onPressed: () async {
                      await showFormDialog(context, true, TagGroupModel());
                    },
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: DataTable(
                          showCheckboxColumn: false,
                          headingRowColor: MaterialStateColor.resolveWith(
                              (states) => AppColors.tableHeaderColor),
                          sortAscending: isAscending,
                          sortColumnIndex: sortColumnIndex,
                          columns: getColumns(Responsive.isDesktop(context)
                              ? columns
                              : columnsMobile),
                          rows: Responsive.isDesktop(context)
                              ? getRows(listTagGroup)
                              : getRowsMobile(listTagGroup),
                        ),
                      ),
                    ],
                  )
                ],
              )),
    );
  }
}

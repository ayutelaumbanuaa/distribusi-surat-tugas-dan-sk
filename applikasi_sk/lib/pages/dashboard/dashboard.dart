import 'dart:convert';

import 'package:applikasi_sk/models/management_user_model.dart';
import 'package:applikasi_sk/pages/DocumentGrup/document_grup.dart';
import 'package:applikasi_sk/pages/ManagementUser/management_user.dart';
import 'package:applikasi_sk/pages/surat_tugas/surat_tugas.dart';
import 'package:applikasi_sk/pages/surat_keputusan/surat_keputusan.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../api_services/ManagementUserServices.dart';
import '../../helper/color.dart';
import '../../helper/constanta.dart';
import '../../helper/dialogs.dart';
import '../../helper/responsive.dart';
import '../widgets/app_bar_action_item.dart';
import '../widgets/side_menu.dart';
import 'home.dart';

class DashboardPage extends StatefulWidget {
  final VoidCallback onSignedOut;
  const DashboardPage({Key? key, required this.onSignedOut}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  List<Widget> listScreen = [];
  int currentMenu = 0;
  late List<ManagementUserModel> listManagementUser = [];
  bool isLoading = false;
  String currentUid = '';

  void initData() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    currentUid = prefs.getString(Constanta.keyUserId)!;
    // print(currentUid);

    listManagementUser = [];
    Map map = {"uid": currentUid};
    var requestBody = jsonEncode(map);
    ManagementUserServices.getDataManagementUser(requestBody).then((result) {
      if (result.status.toLowerCase() == "success") {
        listManagementUser = List<ManagementUserModel>.from(
            result.data.map((x) => ManagementUserModel.fromJson(x)));

        setState(() {
          isLoading = false;
        });
        print(listManagementUser
            .firstWhere((element) => element.tag == currentUid)
            .isAdmin);
      } else {
        //Dialogs.showAlertMessage(result.message, context);
      }
    }).onError((error, stackTrace) {
      Dialogs.showAlertMessage(
          error.toString() + stackTrace.toString(), context);
    });
  }

  void goToTest() {}
  @override
  void initState() {
    initData();
    listScreen = [
      HomePage(goToSuratTugasPage: () {
        setState(() {
          currentMenu = 1;
        });
      }, goToSuratKeputusanPage: () {
        setState(() {
          currentMenu = 2;
        });
      }, goToDocumentGrupPage: () {
        setState(() {
          currentMenu = 3;
        });
      }, goToManagementUserPage: () {
        setState(() {
          currentMenu = 4;
        });
      }),
      const SuratTugasPage(),
      const SuratKeputusanPage(),
      const TagGroupPage(),
      const ManagementUserPage()
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? const Scaffold(
            body: Center(child: CircularProgressIndicator()),
          )
        : Scaffold(
            key: _drawerKey,
            drawer: SizedBox(
                width: 100,
                child: SideMenu(
                  isAdmin: listManagementUser
                          .firstWhere((element) => element.tag == currentUid)
                          .isAdmin ??
                      'Dosen',
                  logout: widget.onSignedOut,
                  goToSuratTugasPage: () {
                    setState(() {
                      currentMenu = 1;
                    });
                  },
                  goToDashboardPage: () {
                    setState(() {
                      currentMenu = 0;
                    });
                  },
                  goToSuratKeputusanPage: () {
                    setState(() {
                      currentMenu = 2;
                    });
                  },
                  goToDocumentGrupPage: () {
                    setState(() {
                      currentMenu = 3;
                    });
                  },
                  goToManagementUserPage: () {
                    setState(() {
                      currentMenu = 4;
                    });
                  },
                )),
            appBar: !Responsive.isDesktop(context)
                ? AppBar(
                    elevation: 0,
                    backgroundColor: AppColors.white,
                    leading: IconButton(
                        onPressed: () {
                          _drawerKey.currentState!.openDrawer();
                        },
                        icon: const Icon(Icons.menu, color: AppColors.black)),
                    actions: [
                      AppBarActionItems(),
                    ],
                  )
                : const PreferredSize(
                    preferredSize: Size.zero,
                    child: SizedBox(),
                  ),
            body: SafeArea(
                child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (Responsive.isDesktop(context))
                  Expanded(
                    flex: 1,
                    child: SideMenu(
                      isAdmin: listManagementUser
                              .firstWhere(
                                  (element) => element.tag == currentUid)
                              .isAdmin ??
                          'Dosen',
                      logout: widget.onSignedOut,
                      goToSuratTugasPage: () {
                        setState(() {
                          currentMenu = 1;
                        });
                      },
                      goToDashboardPage: () {
                        setState(() {
                          currentMenu = 0;
                        });
                      },
                      goToSuratKeputusanPage: () {
                        setState(() {
                          currentMenu = 2;
                        });
                      },
                      goToDocumentGrupPage: () {
                        setState(() {
                          currentMenu = 3;
                        });
                      },
                      goToManagementUserPage: () {
                        setState(() {
                          currentMenu = 4;
                        });
                      },
                    ),
                  ),
                Expanded(
                    flex: 10, child: SafeArea(child: listScreen[currentMenu]))
              ],
            )),
          );
  }
}

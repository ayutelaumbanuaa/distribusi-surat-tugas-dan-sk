import 'dart:convert';
import 'package:applikasi_sk/models/management_user_model.dart';
import 'package:applikasi_sk/pages/dashboard/calendar.dart';
import 'package:applikasi_sk/pages/dashboard/recent_file.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../api_services/ManagementUserServices.dart';
import '../../helper/constanta.dart';
import '../../helper/dialogs.dart';
import '../../helper/responsive.dart';
import '../widgets/header.dart';

class HomePage extends StatefulWidget {
  final VoidCallback goToSuratTugasPage;
  final VoidCallback goToSuratKeputusanPage;
  final VoidCallback goToDocumentGrupPage;
  final VoidCallback goToManagementUserPage;
  const HomePage({
    Key? key,
    required this.goToSuratTugasPage,
    required this.goToSuratKeputusanPage,
    required this.goToDocumentGrupPage,
    required this.goToManagementUserPage,
  }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  List<Widget> listScreen = [];
  int currentMenu = 0;
  late List<ManagementUserModel> listManagementUser = [];
  bool isLoading = false;
  String currentUid = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 25, horizontal: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Header(
                title: 'Dashboard',
                subTitle: '',
              ),
              GridView(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                    crossAxisSpacing: 5,
                    mainAxisSpacing: 5,
                    mainAxisExtent: 80,
                    childAspectRatio: 1),
                children: [
                  Container(
                      width: 100,
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4),
                        ),
                        color: Colors.white,
                        elevation: 10,
                        child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              GestureDetector(
                                onTap: () {
                                  widget.goToSuratTugasPage();
                                },
                                child: const ListTile(
                                  leading: Icon(
                                    Icons.library_books,
                                    size: 50,
                                    color: Color.fromARGB(255, 59, 59, 59),
                                  ),
                                  title: Text('Surat Tugas',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold)),
                                  subtitle: Text('Value',
                                      style: TextStyle(color: Colors.black)),
                                ),
                              )
                            ]),
                      )),
                  Container(
                      width: 100,
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4),
                        ),
                        color: Colors.white,
                        elevation: 10,
                        child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              GestureDetector(
                                onTap: () {
                                  widget.goToSuratKeputusanPage();
                                },
                                child: const ListTile(
                                  leading: Icon(
                                    Icons.attach_email_outlined,
                                    size: 50,
                                    color: Color.fromARGB(255, 59, 59, 59),
                                  ),
                                  title: Text('Surat Keputusan',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold)),
                                  subtitle: Text('Value',
                                      style: TextStyle(color: Colors.black)),
                                ),
                              )
                            ]),
                      )),
                ],
                padding: EdgeInsets.all(10),
                shrinkWrap: true,
                reverse: true,
              ),
              Text(
                'Recents File',
                style: TextStyle(
                  fontSize: 19,
                  fontWeight: FontWeight.w700,
                  color: Colors.black,
                ),
              ),
              Column(
                children: [
                  Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 5,
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 12,
                                ),
                                Row(
                                  children: [
                                    if (!Responsive.isMobile(context))
                                      Expanded(
                                        child: RecentFile(),
                                        flex: 5,
                                      ),
                                    if (!Responsive.isMobile(context))
                                      SizedBox(
                                        width: 12,
                                      ),
                                    Expanded(
                                      flex: 2,
                                      child: CalendarWidget(),
                                    ),
                                  ],
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                ),
                                SizedBox(
                                  height: 12,
                                ),
                                if (Responsive.isMobile(context)) RecentFile(),
                                SizedBox(
                                  height: 12,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ],
          )),
    );
  }

  void goToDashboardPage() {}

  void logout() {}
}

void goToSuratTugasPage() {}

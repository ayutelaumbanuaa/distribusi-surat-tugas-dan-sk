import 'dart:convert';

import 'package:applikasi_sk/helper/responsive.dart';
import 'package:applikasi_sk/models/data_mahasiswa_model.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../api_services/LDAPServices.dart';
import '../../animation/fadeanimation.dart';
import '../../helper/constanta.dart';
import '../../helper/dialogs.dart';

class LoginPage extends StatefulWidget {
  final VoidCallback onSignedIn;
  const LoginPage({Key? key, required this.onSignedIn}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Color enabled = const Color(0xFF827F8A);
  Color enabledtxt = Colors.white;
  Color grey = Colors.grey;
  Color backgroundColor = const Color(0xFF1F1A30);
  bool ispasswordev = true;
  bool isLoading = false;
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var we = MediaQuery.of(context).size.width;
    var he = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SingleChildScrollView(
          child: SizedBox(
              width: we,
              height: he,
              child: Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  FadeAnimation(
                      delay: 0.8,
                      child: Image.asset(
                        'assets/images/login.png',
                        width: MediaQuery.of(context).size.width * 0.9,
                        height: MediaQuery.of(context).size.height * 0.4,
                      )),
                  FadeAnimation(
                      delay: 1,
                      child: Container(
                        margin: const EdgeInsets.only(right: 230.0),
                        child: Text(
                          "Login",
                          style: GoogleFonts.heebo(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 35,
                              letterSpacing: 2),
                        ),
                      )),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                  FadeAnimation(
                    delay: 1,
                    child: Container(
                      margin: const EdgeInsets.only(right: 150.0),
                      child: Text(
                        "Please sign in to continue",
                        style: GoogleFonts.heebo(
                            color: Colors.grey, letterSpacing: 0.5),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.04,
                  ),
                  FadeAnimation(
                    delay: 1,
                    child: Container(
                        width: Responsive.isMobile(context)
                            ? MediaQuery.of(context).size.width * 0.9
                            : 350,
                        decoration: BoxDecoration(
                          color: grey,
                        ),
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          controller: _usernameController,
                          obscureText: false,
                          validator: (text) {
                            if (text == null || text.isEmpty) {
                              return 'Email is empty';
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                            icon: Icon(Icons.email, color: Colors.white),
                            border: InputBorder.none,
                            hintText: "Your Username",
                            labelText: "Username",
                          ),
                        )),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  FadeAnimation(
                    delay: 1,
                    child: Container(
                        width: Responsive.isMobile(context)
                            ? MediaQuery.of(context).size.width * 0.9
                            : 350,
                        decoration: BoxDecoration(
                          color: grey,
                        ),
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          controller: _passwordController,
                          obscureText: true,
                          validator: (text) {
                            if (text == null || text.isEmpty) {
                              return 'Password is empty';
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                            hintText: "Your Password",
                            labelText: "Password",
                            border: InputBorder.none,
                            icon: Icon(
                              Icons.key,
                              color: Colors.white,
                            ),
                          ),
                        )),
                  ),
                  SizedBox(
                    height: he * 0.02,
                  ),
                  FadeAnimation(
                    delay: 1,
                    child: ElevatedButton(
                        onPressed: () {
                          login();
                        },
                        child: Text(
                          "Login",
                          style: GoogleFonts.heebo(
                            color: Colors.white,
                            letterSpacing: 0.5,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        style: TextButton.styleFrom(
                            backgroundColor: Color.fromARGB(255, 38, 19, 148),
                            padding: const EdgeInsets.symmetric(
                                vertical: 15.0, horizontal: 80),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0)))),
                  ),
                ],
              ))),
    );
  }

  void login() async {
    setState(() {
      isLoading = true;
    });
    var map = <String, dynamic>{};
    map['username'] = _usernameController.text;
    map['password'] = _passwordController.text;

    var requestBody = jsonEncode(map);
    LDAPServices.login(requestBody).then((result) async {
      // print(result.toJson().toString());
      if (result.status == "success") {
        DataMahasiswaModel data = DataMahasiswaModel.fromJson(result.data);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString(Constanta.keyUserId, data.nimNikUnit);
        widget.onSignedIn();
        Dialogs.showAlertMessage(result.message, context);
        // print(model.toJson().toString());
      } else {
        Dialogs.showAlertMessage(result.message, context);
      }

      setState(() {
        isLoading = false;
      });
    });
  }
}

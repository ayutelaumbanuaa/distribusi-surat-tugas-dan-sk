import 'dart:convert';
import 'dart:typed_data';
import 'dart:html' as html;
import 'package:applikasi_sk/models/tag_group_model.dart';
import 'package:flutter_chips_input/flutter_chips_input.dart';
import 'package:applikasi_sk/api_services/LDAPServices.dart';
import 'package:applikasi_sk/api_services/SuratTugasServices.dart';
import 'package:applikasi_sk/helper/dialogs.dart';
import 'package:applikasi_sk/models/apiresponse.dart';
import 'package:applikasi_sk/models/data_dosen_model.dart';
import 'package:applikasi_sk/models/surat_tugas_model.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../api_services/ManagementUserServices.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../../api_services/TagGroupServices.dart';
import '../../helper/color.dart';
import '../../helper/constanta.dart';
import '../../helper/responsive.dart';
import '../../models/management_user_model.dart';
import '../widgets/header.dart';

class SuratTugasPage extends StatefulWidget {
  const SuratTugasPage({Key? key}) : super(key: key);

  @override
  State<SuratTugasPage> createState() => _SuratTugasPageState();
}

class _SuratTugasPageState extends State<SuratTugasPage> {
  bool isLoading = false;
  late List<SuratTugasModel> listSuratTugas = [];
  late List<DataDosenModel> listDataDosen = [];
  late List<TagGroupModel> listGroup = [];
  int? sortColumnIndex;
  bool isAscending = false;
  final columns = [
    'ID',
    'No Surat',
    'Deskripsi',
    'Tag',
    'Created By',
    'Download'
  ];
  final columnsMobile = ['No Surat', 'Tag', 'Download'];
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String currentUid = '';
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  List<Widget> listScreen = [];
  int currentMenu = 0;
  late List<ManagementUserModel> listManagementUser = [];

  void initDataUser() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    currentUid = prefs.getString(Constanta.keyUserId)!;
    // print(currentUid);

    listManagementUser = [];
    Map map = {"uid": currentUid};
    var requestBody = jsonEncode(map);
    ManagementUserServices.getDataManagementUser(requestBody).then((result) {
      if (result.status.toLowerCase() == "success") {
        listManagementUser = List<ManagementUserModel>.from(
            result.data.map((x) => ManagementUserModel.fromJson(x)));

        setState(() {
          isLoading = false;
        });
        print(listManagementUser
            .firstWhere((element) => element.tag == currentUid)
            .isAdmin);
      } else {
        //Dialogs.showAlertMessage(result.message, context);
      }
    }).onError((error, stackTrace) {
      Dialogs.showAlertMessage(
          error.toString() + stackTrace.toString(), context);
    });
  }

  void downloadFile(String url) {
    html.AnchorElement anchorElement = html.AnchorElement(href: url);
    anchorElement.download = url;
    anchorElement.click();
  }

  List<DataColumn> getColumns(List<String> columns) => columns
      .map((String column) => DataColumn(
            label: Text(
              column,
              style: const TextStyle(color: Colors.white),
            ),
            onSort: onSort,
          ))
      .toList();

  List<DataRow> getRowsMobile(List<SuratTugasModel> objects) =>
      objects.map((SuratTugasModel object) {
        var index = objects.indexOf(object);
        final cells = [object.nama, object.tag];
        List<DataCell> listDataCell = getCells(cells);
        listDataCell.add(DataCell(IconButton(
            icon: const Icon(Icons.download),
            onPressed: () async {
              downloadFile(Constanta.apiUrl +
                  "download.php?file=" +
                  object.filePath!.replaceAll("/uploads/", ""));
            })));
        return DataRow(
            color: index % 2 == 0
                ? MaterialStateColor.resolveWith(
                    (states) => AppColors.tableCellOddColor)
                : MaterialStateColor.resolveWith(
                    (states) => AppColors.tableCellEvenColor),
            onSelectChanged: (bool? selected) async {
              if (selected!) {
                await showFormDialog(context, false, object);
              }
            },
            cells: listDataCell);
      }).toList();

  List<DataRow> getRows(List<SuratTugasModel> objects) =>
      objects.map((SuratTugasModel object) {
        var index = objects.indexOf(object);
        final cells = [
          object.id,
          object.nama,
          object.deskripsi,
          object.tag,
          object.createdBy
        ];
        List<DataCell> listDataCell = getCells(cells);
        listDataCell.add(DataCell(IconButton(
            icon: const Icon(Icons.download),
            onPressed: () async {
              downloadFile(Constanta.apiUrl +
                  "download.php?file=" +
                  object.filePath!.replaceAll("/uploads/", ""));
            })));

        return DataRow(
            color: index % 2 == 0
                ? MaterialStateColor.resolveWith(
                    (states) => AppColors.tableCellOddColor)
                : MaterialStateColor.resolveWith(
                    (states) => AppColors.tableCellEvenColor),
            onSelectChanged: (bool? selected) async {
              if (selected!) {
                await showFormDialog(context, false, object);
              }
            },
            cells: listDataCell);
      }).toList();

  List<DataCell> getCells(List<dynamic> cells) =>
      cells.map((data) => DataCell(Text('$data'))).toList();

  void onSort(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      listSuratTugas.sort((obj1, obj2) =>
          compareString(ascending, obj1.id.toString(), obj2.id.toString()));
    } else if (columnIndex == 1) {
      listSuratTugas.sort(
          (obj1, obj2) => compareString(ascending, obj1.nama!, obj2.nama!));
    } else if (columnIndex == 2) {
      listSuratTugas.sort((obj1, obj2) =>
          compareString(ascending, obj1.deskripsi!, obj2.deskripsi!));
    } else if (columnIndex == 3) {
      listSuratTugas
          .sort((obj1, obj2) => compareString(ascending, obj1.tag!, obj2.tag!));
    } else if (columnIndex == 4) {
      listSuratTugas.sort((obj1, obj2) =>
          compareString(ascending, obj1.createdBy!, obj2.createdBy!));
    }
    setState(() {
      sortColumnIndex = columnIndex;
      isAscending = ascending;
    });
  }

  int compareString(bool ascending, String value1, String value2) =>
      ascending ? value1.compareTo(value2) : value2.compareTo(value1);

  Future<void> showFormDialog(
      BuildContext context, bool isNewData, SuratTugasModel model) async {
    var isAdminKah = listManagementUser
            .firstWhere((element) => element.tag == currentUid)
            .isAdmin ??
        'Dosen';

    return isAdminKah == 'Admin'
        ? await showDialog(
            barrierDismissible: false,
            context: context,
            builder: (context) {
              bool isLoading = false;
              final _nameController = TextEditingController();
              final _deskripsiController = TextEditingController();
              final _tagController = TextEditingController();

              final _fileController = TextEditingController();
              late Uint8List uploadedFile;
              late FilePickerResult? result;
              late List<DataDosenModel> listTag = [];
              late List<TagGroupModel> listTagGroup = [];
              if (!isNewData) {
                var parts = model.tag!.split('||');
                for (var niknimunit in parts) {
                  listTag.add(listDataDosen
                      .firstWhere((data) => data.nimNikUnit == niknimunit));
                }
              }
              if (!isNewData) _nameController.text = model.nama!;
              if (!isNewData) _deskripsiController.text = model.deskripsi!;
              if (!isNewData) _tagController.text = model.tag!;

              return StatefulBuilder(builder: (context, setState) {
                return AlertDialog(
                  insetPadding: const EdgeInsets.all(8.0),
                  content: SizedBox(
                    width: Responsive.isDesktop(context)
                        ? MediaQuery.of(context).size.width * 0.6
                        : Responsive.isTablet(context)
                            ? MediaQuery.of(context).size.width * 0.8
                            : MediaQuery.of(context).size.width,
                    child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "Data Surat Tugas",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 24),
                            ),
                            const SizedBox(height: 18),
                            TextFormField(
                              controller: _nameController,
                              validator: (value) {
                                return value!.isNotEmpty
                                    ? null
                                    : "Invalid Field";
                              },
                              decoration: const InputDecoration(
                                  hintText: "Enter Some Text",
                                  labelText: "No Surat"),
                            ),
                            const SizedBox(height: 12),
                            TextFormField(
                              controller: _deskripsiController,
                              validator: (value) {
                                return value!.isNotEmpty
                                    ? null
                                    : "Invalid Field";
                              },
                              decoration: const InputDecoration(
                                  hintText: "Enter Some Text",
                                  labelText: "Deskripsi Dokumen"),
                            ),
                            const SizedBox(height: 12),
                            ChipsInput(
                              initialValue: isNewData ? [] : listTag,
                              decoration: InputDecoration(
                                labelText: "Select People to tag",
                              ),
                              maxChips: 100,
                              findSuggestions: (String query) {
                                if (query.length != 0) {
                                  var lowercaseQuery = query.toLowerCase();
                                  return listDataDosen.where((profile) {
                                    return profile.name
                                        .toLowerCase()
                                        .contains(query.toLowerCase());
                                  }).toList(growable: false)
                                    ..sort((a, b) => a.name
                                        .toLowerCase()
                                        .indexOf(lowercaseQuery)
                                        .compareTo(b.name
                                            .toLowerCase()
                                            .indexOf(lowercaseQuery)));
                                } else {
                                  return const <DataDosenModel>[];
                                }
                              },
                              onChanged: (listdata) {
                                _tagController.text = "";
                                for (var element in listdata) {
                                  DataDosenModel data =
                                      element as DataDosenModel;
                                  if (_tagController.text == "") {
                                    _tagController.text = data.nimNikUnit;
                                  } else {
                                    _tagController.text = _tagController.text +
                                        "||" +
                                        data.nimNikUnit;
                                  }
                                }

                                // print(_tagController.text);
                              },
                              chipBuilder: (context, state, profile) {
                                DataDosenModel model =
                                    profile as DataDosenModel;
                                return InputChip(
                                  key: ObjectKey(profile),
                                  label: Text(model.name),
                                  onDeleted: () => state.deleteChip(profile),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                );
                              },
                              suggestionBuilder: (context, state, profile) {
                                DataDosenModel model =
                                    profile as DataDosenModel;
                                return ListTile(
                                  key: ObjectKey(profile),
                                  title: Text(model.name),
                                  subtitle: Text(model.nimNikUnit),
                                  onTap: () => state.selectSuggestion(profile),
                                );
                              },
                            ),
                            ChipsInput(
                              initialValue: isNewData ? [] : listTagGroup,
                              decoration: InputDecoration(
                                labelText: "Select Group to tag",
                              ),
                              maxChips: 100,
                              findSuggestions: (String query) {
                                if (query.length != 0) {
                                  var lowercaseQuery = query.toLowerCase();
                                  return listGroup.where((profile) {
                                    return profile.namaGroup!
                                        .toLowerCase()
                                        .contains(query.toLowerCase());
                                  }).toList(growable: false)
                                    ..sort((a, b) => a.namaGroup!
                                        .toLowerCase()
                                        .indexOf(lowercaseQuery)
                                        .compareTo(b.namaGroup!
                                            .toLowerCase()
                                            .indexOf(lowercaseQuery)));
                                } else {
                                  return const <TagGroupModel>[];
                                }
                              },
                              onChanged: (listdata) {
                                _tagController.text = "";
                                for (var element in listdata) {
                                  TagGroupModel data = element as TagGroupModel;
                                  if (_tagController.text == "") {
                                    _tagController.text = data.tag!;
                                  } else {
                                    _tagController.text =
                                        _tagController.text + "||" + data.tag!;
                                  }
                                }

                                print(_tagController.text);
                              },
                              chipBuilder: (context, state, profile) {
                                TagGroupModel model = profile as TagGroupModel;
                                return InputChip(
                                  key: ObjectKey(profile),
                                  label: Text(model.namaGroup!),
                                  onDeleted: () => state.deleteChip(profile),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                );
                              },
                              suggestionBuilder: (context, state, profile) {
                                TagGroupModel model = profile as TagGroupModel;
                                return ListTile(
                                  key: ObjectKey(profile),
                                  title: Text(model.namaGroup!),
                                  subtitle: Text(model.tag!),
                                  onTap: () => state.selectSuggestion(profile),
                                );
                              },
                            ),
                            const SizedBox(height: 12),
                            TextFormField(
                              readOnly: true,
                              controller: _fileController,
                              onTap: () async {
                                result = await FilePicker.platform.pickFiles();
                                if (result != null) {
                                  setState(() {
                                    uploadedFile = result!.files.first.bytes!;
                                  });
                                  _fileController.text =
                                      result!.files.first.name;
                                }
                              },
                              validator: (value) {
                                return isNewData
                                    ? value!.isNotEmpty
                                        ? null
                                        : "Invalid Field"
                                    : null;
                              },
                              decoration: const InputDecoration(
                                  hintText: "Select File Document",
                                  labelText: "File Dokumen"),
                            ),
                            const SizedBox(height: 12),
                          ],
                        )),
                  ),
                  actions: <Widget>[
                    if (!isNewData)
                      TextButton(
                        child: const Text(
                          'Delete',
                          style: TextStyle(color: Colors.red),
                        ),
                        onPressed: () async {
                          bool? isValid = await Dialogs.showOptionsDialog(
                              "Are you sure to delete this data?",
                              "YES",
                              "NO",
                              false,
                              context);
                          if (isValid!) {
                            Navigator.of(context).pop();
                            var requestBody = jsonEncode(model.toJson());
                            SuratTugasServices.deleteDataSuratTugas(requestBody)
                                .then((result) {
                              if (result.status.toLowerCase() == 'success') {
                                Dialogs.showAlertMessage(
                                    result.message, context);
                              }
                            }).whenComplete(() {
                              initData();
                            });
                          }
                        },
                      ),
                    TextButton(
                      child: const Text('Cancel'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    isLoading
                        ? const CircularProgressIndicator()
                        : TextButton(
                            child: Text(isNewData ? 'Add' : 'Update'),
                            onPressed: () async {
                              if (_formKey.currentState!.validate()) {
                                if (!mounted) return;
                                setState(() {
                                  isLoading = true;
                                });
                                var request = http.MultipartRequest(
                                    'POST',
                                    Uri.parse(Constanta.apiUrl +
                                        '/surattugas/save.php'));
                                request.fields['save_type'] =
                                    isNewData ? 'add' : 'update';
                                request.fields['id'] =
                                    isNewData ? '' : model.id!;
                                request.fields['file_path'] =
                                    isNewData ? '' : model.filePath!;
                                request.fields['nama'] = _nameController.text;
                                request.fields['deskripsi'] =
                                    _deskripsiController.text;
                                request.fields['tag'] = _tagController.text;

                                request.fields['created_by'] = currentUid;
                                request.fields['created_date'] =
                                    DateTime.now().toIso8601String();
                                request.fields['updated_by'] = currentUid;
                                request.fields['updated_date'] =
                                    DateTime.now().toIso8601String();
                                request.fields['formated_date'] =
                                    DateFormat('yyyyMMddHHmmss')
                                        .format(DateTime.now());
                                if (_fileController.text.isNotEmpty) {
                                  List<int> list = uploadedFile.cast();
                                  request.files.add(
                                      http.MultipartFile.fromBytes(
                                          'fileToUpload', list,
                                          filename: result!.files.first.name));
                                }
                                http.StreamedResponse response =
                                    await request.send();
                                setState(() {
                                  isLoading = false;
                                });
                                if (response.statusCode == 200) {
                                  initData();
                                  Navigator.of(context).pop();
                                  // print(await response.stream.bytesToString());
                                  ApiResponse apiResponse = apiResponseFromJson(
                                      await response.stream.bytesToString());
                                  Dialogs.showAlertMessage(
                                      "" + apiResponse.message, context);
                                } else {
                                  Dialogs.showAlertMessage(
                                      "Document Error : " +
                                          response.reasonPhrase!,
                                      context);
                                }
                              }
                            },
                          ),
                  ],
                );
              });
            })
        : Dialogs.showAlertMessage("Anda Bukan Admin", context);
  }

  @override
  void initState() {
    initData();
    initDataUser();
    super.initState();
  }

  void initData() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    currentUid = prefs.getString(Constanta.keyUserId)!;

    listSuratTugas = [];
    Map map = {"uid": currentUid};
    var requestBody = jsonEncode(map);
    SuratTugasServices.getDataSuratTugas(requestBody).then((result) {
      setState(() {
        isLoading = false;
      });
      if (result.status.toLowerCase() == "success") {
        listSuratTugas = List<SuratTugasModel>.from(
            result.data.map((x) => SuratTugasModel.fromJson(x)));
      } else {
        //Dialogs.showAlertMessage(result.message, context);
      }
    }).onError((error, stackTrace) {
      Dialogs.showAlertMessage(
          error.toString() + stackTrace.toString(), context);
    });

    LDAPServices.getListDataDosen(requestBody).then((result) {
      setState(() {
        isLoading = false;
      });
      if (result.status.toLowerCase() == "success") {
        listDataDosen = List<DataDosenModel>.from(
            result.data.map((x) => DataDosenModel.fromJson(x)));
      } else {
        //Dialogs.showAlertMessage(result.message, context);
      }
    }).onError((error, stackTrace) {
      Dialogs.showAlertMessage(
          error.toString() + stackTrace.toString(), context);
    });
    TagGroupServices.getDataTagGroup(requestBody).then((result) {
      setState(() {
        isLoading = false;
      });
      if (result.status.toLowerCase() == "success") {
        listGroup = List<TagGroupModel>.from(
            result.data.map((x) => TagGroupModel.fromJson(x)));
      } else {
        //Dialogs.showAlertMessage(result.message, context);
      }
    }).onError((error, stackTrace) {
      Dialogs.showAlertMessage(
          error.toString() + stackTrace.toString(), context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Header(
                    title: 'Surat Tugas',
                    subTitle: '',
                  ),
                  ElevatedButton.icon(
                    label: Text(
                      'Tambahkan Surat',
                    ),
                    icon: Icon(Icons.add),
                    style: ElevatedButton.styleFrom(
                      primary: Color.fromARGB(255, 74, 127, 207),
                    ),
                    onPressed: () async {
                      await showFormDialog(context, true, SuratTugasModel());
                    },
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: DataTable(
                          showCheckboxColumn: false,
                          headingRowColor: MaterialStateColor.resolveWith(
                              (states) => AppColors.tableHeaderColor),
                          sortAscending: isAscending,
                          sortColumnIndex: sortColumnIndex,
                          columns: getColumns(Responsive.isDesktop(context)
                              ? columns
                              : columnsMobile),
                          rows: Responsive.isDesktop(context)
                              ? getRows(listSuratTugas)
                              : getRowsMobile(listSuratTugas),
                        ),
                      ),
                    ],
                  )
                ],
              )),
    );
  }
}

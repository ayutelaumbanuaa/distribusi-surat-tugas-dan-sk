import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../helper/color.dart';
import '../../helper/size_config.dart';

class SideMenu extends StatefulWidget {
  final String isAdmin;
  final VoidCallback logout;
  final VoidCallback goToSuratTugasPage;
  final VoidCallback goToSuratKeputusanPage;
  final VoidCallback goToDashboardPage;
  final VoidCallback goToDocumentGrupPage;
  final VoidCallback goToManagementUserPage;

  const SideMenu({
    Key? key,
    required this.logout,
    required this.goToSuratTugasPage,
    required this.goToDashboardPage,
    required this.goToSuratKeputusanPage,
    required this.goToDocumentGrupPage,
    required this.goToManagementUserPage,
    this.isAdmin = 'Dosen',
  }) : super(key: key);

  @override
  State<SideMenu> createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  @override
  void initState() {
    print('Side menu' + widget.isAdmin);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 0,
      child: Container(
        width: double.infinity,
        height: SizeConfig.screenHeight,
        decoration: const BoxDecoration(color: Color.fromARGB(255, 43, 43, 47)),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset('assets/images/login.png'),
              IconButton(
                  iconSize: 25,
                  icon: const Icon(
                    Icons.maps_home_work,
                    color: AppColors.white,
                  ),
                  onPressed: () {
                    widget.goToDashboardPage();
                  }),
              Text(
                "Home",
                style:
                    GoogleFonts.heebo(color: Colors.white, letterSpacing: 0.5),
              ),
              IconButton(
                  iconSize: 25,
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  icon: const Icon(
                    Icons.library_books,
                    color: AppColors.white,
                  ),
                  onPressed: () {
                    widget.goToSuratTugasPage();
                  }),
              Text(
                "Tugas",
                style:
                    GoogleFonts.heebo(color: Colors.white, letterSpacing: 0.5),
              ),
              IconButton(
                  iconSize: 25,
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  icon: const Icon(
                    Icons.attach_email_outlined,
                    color: AppColors.white,
                  ),
                  onPressed: () {
                    widget.goToSuratKeputusanPage();
                  }),
              Text(
                "Keputusan",
                style:
                    GoogleFonts.heebo(color: Colors.white, letterSpacing: 0.5),
              ),
              widget.isAdmin == 'Admin'
                  ? Column(
                      children: [
                        IconButton(
                            iconSize: 25,
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            icon: const Icon(
                              Icons.speaker_group,
                              color: AppColors.white,
                            ),
                            onPressed: () {
                              widget.goToDocumentGrupPage();
                            }),
                        Text(
                          "Document",
                          style: GoogleFonts.heebo(
                              color: Colors.white, letterSpacing: 0.5),
                        ),
                        Text(
                          "Grup",
                          style: GoogleFonts.heebo(
                              color: Colors.white, letterSpacing: 0.5),
                        ),
                      ],
                    )
                  : Container(),
              widget.isAdmin == 'Admin'
                  ? Column(
                      children: [
                        IconButton(
                            iconSize: 25,
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            icon: const Icon(
                              Icons.manage_accounts,
                              color: AppColors.white,
                            ),
                            onPressed: () {
                              widget.goToManagementUserPage();
                            }),
                        Text(
                          "Management",
                          style: GoogleFonts.heebo(
                              color: Colors.white, letterSpacing: 0.5),
                        ),
                        Text(
                          "User",
                          style: GoogleFonts.heebo(
                              color: Colors.white, letterSpacing: 0.5),
                        ),
                      ],
                    )
                  : Container(),
              IconButton(
                  iconSize: 25,
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  icon: const Icon(
                    Icons.logout,
                    color: AppColors.white,
                  ),
                  onPressed: () {
                    widget.logout();
                  }),
              Text(
                "LogOut",
                style:
                    GoogleFonts.heebo(color: Colors.white, letterSpacing: 0.5),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

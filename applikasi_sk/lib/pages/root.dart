import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../helper/constanta.dart';
import 'dashboard/dashboard.dart';
import 'login/login.dart';

enum AuthStatus { NOT_DETERMINED, NOT_LOGGED_IN, LOGGED_IN }

class RootPage extends StatefulWidget {
  const RootPage({Key? key}) : super(key: key);

  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  bool isLogged = false;

  @override
  void initState() {
    checkLoginState();
    super.initState();
  }

  void checkLoginState() async {
    WidgetsFlutterBinding.ensureInitialized();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var uId = prefs.getString(Constanta.keyUserId);
    if (uId == null || uId.isEmpty) {
      onSignedOut();
    } else {
      onSignedIn();
    }
  }

  void onSignedIn() {
    if (!mounted) return;
    setState(() {
      authStatus = AuthStatus.LOGGED_IN;
    });
  }

  void onSignedOut() async {
    if (!mounted) return;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constanta.keyUserId, '');
    setState(() {
      authStatus = AuthStatus.NOT_LOGGED_IN;
    });
  }

  Widget _waitingScreen() {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: const CircularProgressIndicator(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.NOT_LOGGED_IN:
        {
          return LoginPage(
            onSignedIn: onSignedIn,
          );
        }
      case AuthStatus.LOGGED_IN:
        {
          return DashboardPage(onSignedOut: onSignedOut);
        }
      default:
        return _waitingScreen();
    }
  }
}

import '../helper/constanta.dart';
import '../models/apiresponse.dart';
import 'BaseAPIServices.dart';

class SuratKeputusanServices {
  static String endpoint = Constanta.apiUrl + "suratkeputusan/read.php";
  static String endpoint2 = Constanta.apiUrl + "suratkeputusan/delete.php";

  static Future<ApiResponse> getDataSuratKeputusan(dynamic requestBody) async {
    ApiResponse apiResponse =
        ApiResponse(status: 'default', message: 'default', data: []);
    await BaseAPIService.sendPostRequest(endpoint, "", requestBody)
        .then((value) {
      apiResponse = value;
    });
    return apiResponse;
  }

  static Future<ApiResponse> deleteDataSuratKeputusan(
      dynamic requestBody) async {
    ApiResponse apiResponse =
        ApiResponse(status: 'default', message: 'default', data: []);
    await BaseAPIService.sendPostRequest(endpoint2, "", requestBody)
        .then((value) {
      apiResponse = value;
    });
    return apiResponse;
  }
}

import '../helper/constanta.dart';
import '../models/apiresponse.dart';
import 'BaseAPIServices.dart';

class TagGroupServices {
  static String endpoint = Constanta.apiUrl + "taggroup/read.php";
  static String endpoint2 = Constanta.apiUrl + "taggroup/delete.php";
  static String endpoint3 = Constanta.apiUrl + "taggroup/save.php";

  static Future<ApiResponse> getDataTagGroup(dynamic requestBody) async {
    ApiResponse apiResponse =
        ApiResponse(status: 'default', message: 'default', data: []);
    await BaseAPIService.sendPostRequest(endpoint, "", requestBody)
        .then((value) {
      apiResponse = value;
    });
    return apiResponse;
  }

  static Future<ApiResponse> deleteDataTagGroup(dynamic requestBody) async {
    ApiResponse apiResponse =
        ApiResponse(status: 'default', message: 'default', data: []);
    await BaseAPIService.sendPostRequest(endpoint2, "", requestBody)
        .then((value) {
      apiResponse = value;
    });
    return apiResponse;
  }

  static Future<ApiResponse> saveDataTagGroup(dynamic requestBody) async {
    ApiResponse apiResponse =
        ApiResponse(status: 'default', message: 'default', data: []);
    await BaseAPIService.sendPostRequest(endpoint3, "", requestBody)
        .then((value) {
      apiResponse = value;
    });
    return apiResponse;
  }
}

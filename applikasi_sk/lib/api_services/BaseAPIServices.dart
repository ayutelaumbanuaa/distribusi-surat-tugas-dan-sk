import 'package:http/http.dart' as http;
import '../models/apiresponse.dart';

class BaseAPIService {
  static Future<ApiResponse> sendPostRequest(String url, String authToken, var requestBody) async {
    var uri = Uri.parse(url);
    var client = http.Client();
    try {
      var response = await client.post(uri, body: requestBody).catchError((error) {
        // print(error.toString());
      });
      final String responseString = response.body;
      return apiResponseFromJson(responseString);
    } catch (error) {
      return ApiResponse(status: 'false', message: "Error : " + error.toString(), data: "");
    } finally {
      client.close();
    }
  }
}

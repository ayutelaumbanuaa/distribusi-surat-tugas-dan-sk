import '../helper/constanta.dart';
import '../models/apiresponse.dart';
import 'BaseAPIServices.dart';

class LDAPServices {
  static String endpoint = Constanta.apiUrl + "ldap/auth.php";
  static String endpoint2 = Constanta.apiUrl + "ldap/read_single.php";
  static String endpoint3 = Constanta.apiUrl + "ldap/listdosen.php";

  static Future<ApiResponse> login(dynamic requestBody) async {
    ApiResponse apiResponse = ApiResponse(status: 'default', message: 'default', data: []);
    await BaseAPIService.sendPostRequest(endpoint, "", requestBody).then((value) {
      apiResponse = value;
    });
    return apiResponse;
  }

  static Future<ApiResponse> getDataUser(dynamic requestBody) async {
    ApiResponse apiResponse = ApiResponse(status: 'default', message: 'default', data: []);
    await BaseAPIService.sendPostRequest(endpoint2, "", requestBody).then((value) {
      apiResponse = value;
    });
    return apiResponse;
  }

  static Future<ApiResponse> getListDataDosen(dynamic requestBody) async {
    ApiResponse apiResponse = ApiResponse(status: 'default', message: 'default', data: []);
    await BaseAPIService.sendPostRequest(endpoint3, "", requestBody).then((value) {
      apiResponse = value;
    });
    return apiResponse;
  }
}

import '../helper/constanta.dart';
import '../models/apiresponse.dart';
import 'BaseAPIServices.dart';

class ManagementUserServices {
  static String endpoint = Constanta.apiUrl + "management/read.php";
  static String endpoint2 = Constanta.apiUrl + "management/delete.php";
  static String endpoint3 = Constanta.apiUrl + "management/save.php";

  static Future<ApiResponse> getDataManagementUser(dynamic requestBody) async {
    ApiResponse apiResponse =
        ApiResponse(status: 'default', message: 'default', data: []);
    await BaseAPIService.sendPostRequest(endpoint, "", requestBody)
        .then((value) {
      apiResponse = value;
    });
    return apiResponse;
  }

  static Future<ApiResponse> deleteDataManagementUser(
      dynamic requestBody) async {
    ApiResponse apiResponse =
        ApiResponse(status: 'default', message: 'default', data: []);
    await BaseAPIService.sendPostRequest(endpoint2, "", requestBody)
        .then((value) {
      apiResponse = value;
    });
    return apiResponse;
  }

  static Future<ApiResponse> saveDataManagementUser(dynamic requestBody) async {
    ApiResponse apiResponse =
        ApiResponse(status: 'default', message: 'default', data: []);
    await BaseAPIService.sendPostRequest(endpoint3, "", requestBody)
        .then((value) {
      apiResponse = value;
    });
    return apiResponse;
  }
}

import '../helper/constanta.dart';
import '../models/apiresponse.dart';
import 'BaseAPIServices.dart';

class SuratTugasServices {
  static String endpoint = Constanta.apiUrl + "surattugas/read.php";
  static String endpoint2 = Constanta.apiUrl + "surattugas/delete.php";

  static Future<ApiResponse> getDataSuratTugas(dynamic requestBody) async {
    ApiResponse apiResponse = ApiResponse(status: 'default', message: 'default', data: []);
    await BaseAPIService.sendPostRequest(endpoint, "", requestBody).then((value) {
      apiResponse = value;
    });
    return apiResponse;
  }

  static Future<ApiResponse> deleteDataSuratTugas(dynamic requestBody) async {
    ApiResponse apiResponse = ApiResponse(status: 'default', message: 'default', data: []);
    await BaseAPIService.sendPostRequest(endpoint2, "", requestBody).then((value) {
      apiResponse = value;
    });
    return apiResponse;
  }
}

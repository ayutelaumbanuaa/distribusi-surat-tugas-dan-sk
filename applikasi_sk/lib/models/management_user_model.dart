import 'dart:convert';

ManagementUserModel managementUserModelFromJson(String str) =>
    ManagementUserModel.fromJson(json.decode(str));

String managementUserModelToJson(ManagementUserModel data) =>
    json.encode(data.toJson());

class ManagementUserModel {
  ManagementUserModel({
    this.saveType,
    this.id,
    this.tag,
    this.isAdmin,
    this.createdBy,
    this.createdDate,
    this.updatedBy,
    this.updatedDate,
  });

  String? saveType = "";
  String? id = "";
  String? tag = "";
  String? isAdmin = "";
  String? createdBy = "";
  DateTime? createdDate = DateTime.now();
  String? updatedBy = "";
  DateTime? updatedDate = DateTime.now();

  factory ManagementUserModel.fromJson(Map<String, dynamic> json) =>
      ManagementUserModel(
        id: json["id"],
        tag: json["tag"],
        isAdmin: json["isadmin"],
        createdBy: json["created_by"],
        createdDate: DateTime.parse(json["created_date"]),
        updatedBy: json["updated_by"],
        updatedDate: DateTime.parse(json["updated_date"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "tag": tag,
        "save_type": saveType,
        "isadmin": isAdmin,
        "created_by": createdBy,
        "created_date": createdDate!.toIso8601String(),
        "updated_by": updatedBy,
        "updated_date": updatedDate!.toIso8601String(),
      };
}

import 'dart:convert';

DataDosenModel dataDosenModelFromJson(String str) => DataDosenModel.fromJson(json.decode(str));

String dataDosenModelToJson(DataDosenModel data) => json.encode(data.toJson());

class DataDosenModel {
  DataDosenModel({
    required this.id,
    required this.nimNikUnit,
    required this.username,
    required this.name,
    required this.email,
    required this.jabatan,
  });

  String id;
  String nimNikUnit;
  String username;
  String name;
  String email;
  String jabatan;

  factory DataDosenModel.fromJson(Map<String, dynamic> json) => DataDosenModel(
        id: json["id"],
        nimNikUnit: json["nim_nik_unit"],
        username: json["username"],
        name: json["name"],
        email: json["email"],
        jabatan: json["jabatan"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nim_nik_unit": nimNikUnit,
        "username": username,
        "name": name,
        "email": email,
        "jabatan": jabatan,
      };
}

// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    this.id,
    this.nimNikUnit,
    this.username,
    this.name,
    this.email,
    this.jabatan,
    this.jk,
    this.tmplhr,
    this.tgllhr,
    this.angkatan,
    this.agama,
    this.alamat,
    this.notelp,
    this.status,
    this.kelas,
    this.jenjang,
    this.prodi,
  });

  String? id;
  String? nimNikUnit;
  String? username;
  String? name;
  String? email;
  String? jabatan;
  String? jk;
  String? tmplhr;
  String? tgllhr;
  String? angkatan;
  String? agama;
  String? alamat;
  String? notelp;
  String? status;
  String? kelas;
  String? jenjang;
  String? prodi;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["id"],
        nimNikUnit: json["nim_nik_unit"],
        username: json["username"],
        name: json["name"],
        email: json["email"],
        jabatan: json["jabatan"],
        jk: json["jk"],
        tmplhr: json["tmplhr"],
        tgllhr: json["tgllhr"],
        angkatan: json["angkatan"],
        agama: json["agama"],
        alamat: json["alamat"],
        notelp: json["notelp"],
        status: json["status"],
        kelas: json["kelas"],
        jenjang: json["jenjang"],
        prodi: json["prodi"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nim_nik_unit": nimNikUnit,
        "username": username,
        "name": name,
        "email": email,
        "jabatan": jabatan,
        "jk": jk,
        "tmplhr": tmplhr,
        "tgllhr": tgllhr,
        "angkatan": angkatan,
        "agama": agama,
        "alamat": alamat,
        "notelp": notelp,
        "status": status,
        "kelas": kelas,
        "jenjang": jenjang,
        "prodi": prodi,
      };
}

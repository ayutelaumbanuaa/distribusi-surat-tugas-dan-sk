import 'dart:convert';

DataMahasiswaModel dataMahasiswaModelFromJson(String str) => DataMahasiswaModel.fromJson(json.decode(str));

String dataMahasiswaModelToJson(DataMahasiswaModel data) => json.encode(data.toJson());

class DataMahasiswaModel {
  DataMahasiswaModel({
    required this.id,
    required this.nimNikUnit,
    required this.username,
    required this.name,
    required this.email,
    required this.jabatan,
    required this.jk,
    required this.tmplhr,
    required this.tgllhr,
    required this.angkatan,
    required this.agama,
    required this.alamat,
    required this.notelp,
    required this.status,
    required this.kelas,
    required this.jenjang,
    required this.prodi,
  });

  String id;
  String nimNikUnit;
  String username;
  String name;
  String email;
  String jabatan;
  String jk;
  String tmplhr;
  String tgllhr;
  String angkatan;
  String agama;
  String alamat;
  String notelp;
  String status;
  String kelas;
  String jenjang;
  String prodi;

  factory DataMahasiswaModel.fromJson(Map<String, dynamic> json) => DataMahasiswaModel(
        id: json["id"],
        nimNikUnit: json["nim_nik_unit"],
        username: json["username"],
        name: json["name"],
        email: json["email"],
        jabatan: json["jabatan"],
        jk: json["jk"],
        tmplhr: json["tmplhr"],
        tgllhr: json["tgllhr"],
        angkatan: json["angkatan"],
        agama: json["agama"],
        alamat: json["alamat"],
        notelp: json["notelp"],
        status: json["status"],
        kelas: json["kelas"],
        jenjang: json["jenjang"],
        prodi: json["prodi"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nim_nik_unit": nimNikUnit,
        "username": username,
        "name": name,
        "email": email,
        "jabatan": jabatan,
        "jk": jk,
        "tmplhr": tmplhr,
        "tgllhr": tgllhr,
        "angkatan": angkatan,
        "agama": agama,
        "alamat": alamat,
        "notelp": notelp,
        "status": status,
        "kelas": kelas,
        "jenjang": jenjang,
        "prodi": prodi,
      };
}

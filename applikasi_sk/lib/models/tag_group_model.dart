import 'dart:convert';

TagGroupModel tagGroupModelFromJson(String str) =>
    TagGroupModel.fromJson(json.decode(str));

String tagGroupModelToJson(TagGroupModel data) => json.encode(data.toJson());

class TagGroupModel {
  TagGroupModel({
    this.saveType,
    this.id,
    this.namaGroup,
    this.tag,
    this.createdBy,
    this.createdDate,
    this.updatedBy,
    this.updatedDate,
  });

  String? saveType = "";
  String? id = "";
  String? namaGroup = "";
  String? tag = "";
  String? createdBy = "";
  DateTime? createdDate = DateTime.now();
  String? updatedBy = "";
  DateTime? updatedDate = DateTime.now();

  factory TagGroupModel.fromJson(Map<String, dynamic> json) => TagGroupModel(
        id: json["id"],
        namaGroup: json["nama_group"],
        tag: json["tag"],
        createdBy: json["created_by"],
        createdDate: DateTime.parse(json["created_date"]),
        updatedBy: json["updated_by"],
        updatedDate: DateTime.parse(json["updated_date"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nama_group": namaGroup,
        "tag": tag,
        "save_type": saveType,
        "created_by": createdBy,
        "created_date": createdDate!.toIso8601String(),
        "updated_by": updatedBy,
        "updated_date": updatedDate!.toIso8601String(),
      };
}

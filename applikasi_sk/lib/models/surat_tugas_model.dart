import 'dart:convert';

SuratTugasModel suratTugasModelFromJson(String str) => SuratTugasModel.fromJson(json.decode(str));

String suratTugasModelToJson(SuratTugasModel data) => json.encode(data.toJson());

class SuratTugasModel {
  SuratTugasModel({
    this.id,
    this.nama,
    this.filePath,
    this.deskripsi,
    this.tag,
    this.createdBy,
    this.createdDate,
    this.updatedBy,
    this.updatedDate,
  });

  String? id = "";
  String? nama = "";
  String? filePath = "";
  String? deskripsi = "";
  String? tag = "";
  String? createdBy = "";
  DateTime? createdDate = DateTime.now();
  String? updatedBy = "";
  DateTime? updatedDate = DateTime.now();

  factory SuratTugasModel.fromJson(Map<String, dynamic> json) => SuratTugasModel(
        id: json["id"],
        nama: json["nama"],
        filePath: json["file_path"],
        deskripsi: json["deskripsi"],
        tag: json["tag"],
        createdBy: json["created_by"],
        createdDate: DateTime.parse(json["created_date"]),
        updatedBy: json["updated_by"],
        updatedDate: DateTime.parse(json["updated_date"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nama": nama,
        "file_path": filePath,
        "deskripsi": deskripsi,
        "tag": tag,
        "created_by": createdBy,
        "created_date": createdDate!.toIso8601String(),
        "updated_by": updatedBy,
        "updated_date": updatedDate!.toIso8601String(),
      };
}

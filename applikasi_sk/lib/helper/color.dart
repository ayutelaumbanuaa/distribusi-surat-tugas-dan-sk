import 'package:flutter/material.dart';

class AppColors {
  static const white = Colors.white;
  static const secondary = Color(0xffa6a6a6);
  static const iconGray = Color(0xff767676);
  static const black = Colors.black;
  static const primary = Color(0xff262626);
  static const primaryBg = Color(0xfff5f5fd);
  static const secondaryBg = Color(0xffececf6);
  static const barBg = Color(0xffe3e3ee);
  static Color tableHeaderColor = Color.fromARGB(255, 59, 59, 59);
  static Color tableCellOddColor = Colors.white;
  static Color tableCellEvenColor = Colors.white;
  static Color tableFilterFieldColor = const Color.fromARGB(255, 201, 244, 250);
}

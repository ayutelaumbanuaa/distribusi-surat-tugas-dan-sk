import 'package:flutter/material.dart';

class Dialogs {
  static Future<void> showAlertMessage(String message, BuildContext context) async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(message),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text(
                'OK',
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  static Future<bool?> showOptionsDialog(
      String message, String positiveOption, String negativeOption, bool allowDismiss, BuildContext context) async {
    return showDialog<bool?>(
        context: context,
        barrierDismissible: allowDismiss,
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(message),
                ],
              ),
            ),
            actions: [
              if (negativeOption.isNotEmpty)
                TextButton(
                  child: Text(
                    negativeOption,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                ),
              TextButton(
                  child: Text(
                    positiveOption,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  }),
            ],
          );
        });
  }
}
